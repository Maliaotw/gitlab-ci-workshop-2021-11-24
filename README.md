# DevOpsDays Taipie 2021 - Advanced GitLab CI 工作坊

> 2021-11-24 - Design by Mouson

Source: [GitLab CI Workshop 2021-11-24](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24)

## 一、GitLab CI 重構及重複使用技巧

- [00-專案初始](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/00-init/.gitlab-ci.yml)

- [01-default](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/01-default/.gitlab-ci.yml)

- [02-extract-to-before-script](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/02-extract-to-before-script/.gitlab-ci.yml)

- [03-extract-before-script-to-hidden-job-and-extend-it](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/03-extract-before-script-to-hidden-job-and-extend-it/.gitlab-ci.yml) 

- [04-extract-prepare-process](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/04-extract-prepare-process/.gitlab-ci.yml)

- [04-02-extract-prepare-process](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/04-02-extract-prepare-process/.gitlab-ci.yml)

- [05-1-extract-to-variables](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/05-1-extract-to-variables/.gitlab-ci.yml)

- [05-2-extract-job-to-template](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/05-2-extract-job-to-template/.gitlab-ci.yml)

- [06-extract-to-parallel-matrix](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/06-extract-to-parallel-matrix/.gitlab-ci.yml)


- [07-extract-version-define-to-hidden-job-and-use-it](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/07-extract-version-define-to-hidden-job-and-use-it/.gitlab-ci.yml)

- [08-extract-to-template-yaml](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/08-extract-to-template-yaml/.gitlab-ci.yml)

- [09-used-template-from-other-project](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/09-used-template-from-other-project/.gitlab-ci.yml)

- [09-1-overwrite-job-and-variable](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/09-1-overwrite-job-and-variable/.gitlab-ci.yml)

- [09-2-pick-up-needs-jobs](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/09-2-pick-up-needs-jobs/.gitlab-ci.yml)

## 二、GitLab CI 執行速度調整小體驗

- [10-stage-without-dag-needs](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/10-stage-without-dag-needs/.gitlab-ci.yml)

- [10-1-stage-with-needs](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/10-1-stage-with-needs/.gitlab-ci.yml)

- [11-compare-difference-image](https://gitlab.com/mouson-gitlab-playground/gitlab-ci-workshop-2021-11-24/-/blob/11-compare-difference-image/.gitlab-ci.yml)